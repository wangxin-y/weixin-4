import { defineConfig } from 'vite'
import alias from '@rollup/plugin-alias';
import uni from '@dcloudio/vite-plugin-uni'
import path from "path"

const srcDir = (targetDir = '') => path.join(__dirname,'./src',targetDir)
// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    uni(),
    alias({
      entries: [
        { find: '@', replacement: srcDir() },
        { find: 'utils', replacement: srcDir('utils') }
      ]
    })
  ],
})
