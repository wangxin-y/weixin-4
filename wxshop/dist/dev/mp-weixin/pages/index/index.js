"use strict";
var common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  setup() {
    const methods = {
      toType() {
        common_vendor.index.bw_navigate({
          url: "/pages/detail/index?type=a",
          params: {
            id: 10,
            name: "wxy"
          }
        });
      }
    };
    return {
      ...methods
    };
  },
  data() {
    return {
      title: "Hello"
    };
  },
  onLoad() {
  },
  methods: {}
};
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return {
    a: common_vendor.o((...args) => _ctx.toType && _ctx.toType(...args))
  };
}
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "H:/shixun1/wei/wxshop/src/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
