"use strict";
var common_vendor = require("../common/vendor.js");
const formatUrl = (url, params = {}) => {
  let [pathname, queryString] = url.split("?");
  if (queryString) {
    queryString.replace(/\#\w+$/, "").split("&").map((item) => {
      const [key, val] = item.split("=");
      params[key] = params[key] ? [val, params[key]].toString() : val;
      return item;
    });
  }
  return pathname + "?" + Object.keys(params).map((key) => `${key}=${params[key]}`).join("&");
};
const INDEX_URL = "/pages/index/index ";
const navigate = async ({ url, params, redirect = false, ...arg }) => {
  if (!url) {
    throw new Error("\u8DF3\u8F6C\u9875\u9762\u5FC5\u987B\u4F20\u9012url\u5730\u5740");
  }
  let originUrl = url;
  url = formatUrl(url, params);
  if (redirect) {
    return await common_vendor.index.redirectTo({
      url,
      ...arg
    });
  }
  try {
    const pages = getCurrentPages();
    let delta = -1;
    if (pages.length > 2 && INDEX_URL === url) {
      for (let i = 0; i < pages.length; i++) {
        if (url === pages[i].route) {
          delta = pages.length - i - 1;
          break;
        }
      }
    }
    if (delta == -1) {
      return await common_vendor.index.navigateTo({
        url,
        ...arg
      });
    } else {
      return await common_vendor.index.navigateBack({
        delta
      });
    }
  } catch (error) {
    if (error.errMsg === "navigateTo:fail can not navigateTo a tabbar page") {
      return await common_vendor.index.switchTab({
        url: originUrl
      });
    }
  }
};
common_vendor.index.bw_navigate = navigate;
