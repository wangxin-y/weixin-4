"use strict";
var __defProp = Object.defineProperty;
var __defNormalProp = (obj, key, value) => key in obj ? __defProp(obj, key, { enumerable: true, configurable: true, writable: true, value }) : obj[key] = value;
var __publicField = (obj, key, value) => {
  __defNormalProp(obj, typeof key !== "symbol" ? key + "" : key, value);
  return value;
};
var common_vendor = require("../common/vendor.js");
const _Axios = class {
  constructor(options) {
    __publicField(this, "request", {
      use: (successFun, errorFun) => {
        this.requestSuccessFun = successFun;
        this.requestErrorFun = errorFun;
      }
    });
    __publicField(this, "response", {
      use: (successFun, errorFun) => {
        this.responseSuccessFun = successFun;
        this.responseErrorFun = errorFun;
      }
    });
    this.options = options;
    this.requestSuccessFun = null;
    this.requestErrorFun = null;
    this.responseSuccessFun = null;
    this.responseErrorFun = null;
  }
  static create(options = { baseUrl: "", timeout: 1e4 }) {
    return _Axios._instance ? _Axios._instance : _Axios._instance = new _Axios(options);
  }
  post(url = "", data = {}, config = {}) {
    return this.sendRequest(`${this.options.baseUrl}${url}`, {
      ...config,
      method: "POST",
      data
    });
  }
  get(url = "", config = {}) {
    return this.sendRequest(`${this.options.baseUrl}${url}`, {
      ...config,
      method: "GET"
    });
  }
  async sendRequest(url, config) {
    try {
      let res = null;
      if (this.requestSuccessFun) {
        const requsetConfig = this.requestSuccessFun({
          url,
          header: {},
          ...config
        });
        if (requsetConfig.url && requsetConfig.method) {
          res = await common_vendor.index.request({ ...requsetConfig });
        } else {
          return this.requestErrorFun && this.requestErrorFun({
            errorMsg: "\u8BF7\u6C42\u914D\u7F6E\u7F3A\u5C11\u5FC5\u586B\u53C2\u6570\uFF0C\u5FC5\u987B\u5305\u542Burl\u548Cmethod"
          });
        }
      } else {
        res = await common_vendor.index.request({
          url,
          ...config
        });
      }
      if (this.responseSuccessFun) {
        return this.responseSuccessFun(res);
      }
      return res;
    } catch (error) {
      if (this.responseErrorFun) {
        return this.responseErrorFun(error);
      }
      return error;
    }
  }
};
let Axios = _Axios;
__publicField(Axios, "_instance", null);
const httpTool = Axios.create({
  baseUrl: "https://bjwz.bwie.com/mall4j",
  timeout: 1e4
});
httpTool.request.use((config) => {
  config.header = {
    ...config.header,
    Authorization: `${common_vendor.index.getStorageSync("token_type")}${common_vendor.index.getStorageSync("access_token")}`
  };
  return config;
}, (error) => {
  return Promise.reject(error);
});
httpTool.response.use((response) => {
  if (response.errMsg === "request:ok") {
    return response.data;
  }
  common_vendor.index.showToast({
    title: response.errMsg,
    icon: "none"
  });
}, (error) => {
  common_vendor.index.showToast({
    title: error.errMsg,
    icon: "none"
  });
});
exports.httpTool = httpTool;
