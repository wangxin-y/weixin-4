"use strict";
Object.defineProperties(exports, { __esModule: { value: true }, [Symbol.toStringTag]: { value: "Module" } });
var common_vendor = require("./common/vendor.js");
var api_user = require("./api/user.js");
require("./utils/navigate.js");
require("./utils/http.js");
if (!Math) {
  "./pages/index/index.js";
  "./pages/type/index.js";
  "./pages/liveShop/index.js";
  "./pages/shopCar/index.js";
  "./pages/my/index.js";
  "./pages/detail/index.js";
}
const _sfc_main = {
  onLaunch: function() {
    common_vendor.index.login().then(({ code }) => {
      api_user.login({ principal: code }).then((res) => {
        common_vendor.index.setStorageSync("access_token", res.access_token);
        common_vendor.index.setStorageSync("token_type", res.token_type);
      });
    });
  },
  onShow: function() {
    console.log("App Show");
  },
  onHide: function() {
    console.log("App Hide");
  }
};
var App = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "H:/shixun1/wei/wxshop/src/App.vue"]]);
function createApp() {
  const app = common_vendor.createSSRApp(App);
  return {
    app
  };
}
console.log("app");
createApp().app.mount("#app");
exports.createApp = createApp;
