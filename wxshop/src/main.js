import {
	createSSRApp
} from "vue";
import App from "./App.vue";
import './utils/navigate'
export function createApp() {
	const app = createSSRApp(App);
	return {
		app,
	};
}
console.log('app')
