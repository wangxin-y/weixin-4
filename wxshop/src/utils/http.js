/**
 * [Axios 对uni.request进行二次封装]
 * 支持添加请求前公共参数
 * 支持得到请求后结果
 */
class Axios {
  static _instance = null; // 该变量只基于函数对象定义了一次
  static create(options = { baseUrl: '', timeout: 10000 }) {  // 静态方法
    // 返回值是一个新的axios实例
    // 单例模式
    return Axios._instance ? Axios._instance : (Axios._instance = new Axios(options));
  }
  constructor(options) {
    this.options = options;
    this.requestSuccessFun = null;
    this.requestErrorFun = null;
    this.responseSuccessFun = null;
    this.responseErrorFun = null;
  }
  post(url = '', data = {}, config = {}) {  // 原型方法 通过实例对象
    return this.sendRequest(`${this.options.baseUrl}${url}`, {
      ...config,
      method: 'POST',
      data,
    });
  }
  get(url = '', config = {}) {
    return this.sendRequest(`${this.options.baseUrl}${url}`, {
      ...config,
      method: 'GET',
    });
  }
  async sendRequest(url, config) {
    try {
      // uni.request 去发送请求
      let res = null;
      if (this.requestSuccessFun) {  // 证明设置拦截器
        const requsetConfig = this.requestSuccessFun({
          url,
          header: {},
          ...config
        });
        // 做一个config是否符合规范的判断
        if (requsetConfig.url && requsetConfig.method) {
          res = await uni.request({ ...requsetConfig });
        } else {
          return this.requestErrorFun && this.requestErrorFun({
            errorMsg: '请求配置缺少必填参数，必须包含url和method'
          })
        }
      } else {
        res = await uni.request({
          url,
          ...config
        })
      }
      // response拦截器
      if (this.responseSuccessFun) {
        return this.responseSuccessFun(res);
      }
      return res;
    } catch (error) { // 请求失败的时候
      if (this.responseErrorFun) {
        return this.responseErrorFun(error);
      }
      return error;
    }
  }
  request = {
    use: (successFun, errorFun) => {
      this.requestSuccessFun = successFun;
      this.requestErrorFun = errorFun;
    }
  }
  response = {
    use: (successFun, errorFun) => {
      this.responseSuccessFun = successFun;
      this.responseErrorFun = errorFun;
    }
  }
}

const httpTool = Axios.create({
  baseUrl: 'https://bjwz.bwie.com/mall4j',
  timeout: 10000
})

httpTool.request.use((config) => {
  // 添加公共参数
  config.header = {
    ...config.header,
    Authorization: `${uni.getStorageSync('token_type')}${uni.getStorageSync('access_token')}` // 从本地存储中去取
  }
  return config; // 要返回合法的config对象 必须包含url 和 method
}, (error) => {
  return Promise.reject(error);
})

httpTool.response.use((response) => {
  if (response.errMsg === 'request:ok') { // 成功返回 response.data
    return response.data;
  }
  // 没有成功做提示
  uni.showToast({
    title: response.errMsg,
    icon: 'none'
  })
}, (error) => {
  uni.showToast({
    title: error.errMsg,
    icon: 'none'
  })
})

export default httpTool;